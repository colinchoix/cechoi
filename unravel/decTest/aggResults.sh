#!/bin/bash
files=decTests/*

mkdir decipherments

for file in $files
do
    #echo ${file:9}
    cd $file
    python3 solveUnravel.py ${file:9}* test_result_mapping.gz > ../../decipherments/${file:9}.dec
    cd ../..

done