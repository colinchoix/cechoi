from collections import Counter
import sys 

fd = file( sys.argv[1] )
s = fd.read().lower()
fd.close()
#s = " ".join( s.split( '\n' ) )
#s = [ x for x in s.split( " " ) if x != '' ]
s = ['<s>'] + list(s) + ['</s>']
print(s)
max_order = 4
def print_dict_to_file(filename, d):
    fd = file(filename,"w")
    items = sorted(d.keys())
    for key in items:
        fd.write( "%s\t%s\n" % (key,d[key]) )
    fd.close()

for order in range( 1, max_order + 1 ):
    c = Counter()
    for i in range( 0, len(s) ):
        c[" ".join( s[i:i+order] )] += 1
    print_dict_to_file( "cipher.%sgram"  % order , dict( c ) )
