import sys

if __name__ == "__main__":
    text = open(sys.argv[1]).read().replace("\n"," ")
    notes = text.split()
    
    timeSignature = float(sys.argv[2])
    barsText = ""
    barLength = 0
    count = 0

    for note in notes:
        noteLength = float(note[-3:])
        barLength += noteLength

        if barLength % timeSignature == 0:
            barLength = 0
            barsText += note + " "
            count += 1
        else:
            barsText += note

    print(barsText, end="")
