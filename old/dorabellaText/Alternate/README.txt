dorabella.txt - Original dorabella ciper transcribed
dorabella_lines.txt - Original dorabella transcription separated into 3 dorabella_lines
dorabella_mirror.txt - Original dorabella cipher where the mirrored symbols are surronded by brackets for easier identification

dorabella_distinct.txt - Dorabella cipher where each mirror is replaced with a distinct character
dorabella_alt.txt - Dorabella cipher where repeated mirrors are replaced with the same dictinct character
dorabella_first.txt - Dorabella cipher where the first character of mirrors are preserved while the second charater is removed
dorabella_second.txt - Dorabella cipher where the second character of mirrors are preserved while the first charater is removed
dorabella_mirrorless.txt - Dorabella cipher where the mirrored symbols are removed completly from the text

Files in the folder AltNoSpace contain the alternate transcriptions of Dorabella but all in a single line and without spaces