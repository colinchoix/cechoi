import sys

def formatLines(lines):
    scores = []
    for line in lines:

        if line[1:5] == "RANK":
            x = line.split()
            
            try:
                scores.append([x[0]+" "+x[1],x[2],x[3]])
            except:
                continue

    return scores

def rankIndex(l, lang):
    for c,x in enumerate(l):
        if x[1] == lang:
            return c
    
    return -1

if __name__ == "__main__":

    
    if len(sys.argv) > 2:
    
        lines = open(sys.argv[1]).read().split("\n")
        lines2 = open(sys.argv[2]).read().split("\n")

        original = formatLines(lines)
        compare = formatLines(lines2)

        for c,x in enumerate(compare, 1):
            oRank = rankIndex(original, x[1])
            cRank = rankIndex(compare, x[1])

            change  = oRank - cRank

            if change == 0 :
                print(c, x[1], x[2], "   Change:", change )
            elif change > 0:
                print(c, x[1], x[2], "   Change: +", change )
            else:
                print(c, x[1], x[2], "   Change: -", abs(change) )
    else:
        lines = open(sys.argv[1]).read().split("\n")
        ranked = formatLines(lines)

        for x in range(len(ranked)):
            print(x+1, ranked[x][1], ranked[x][2])
    