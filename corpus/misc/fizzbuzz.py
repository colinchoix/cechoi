fizz = 2
buzz = 5

for x in range(1,101):
    if x % fizz == 0 and x % buzz == 0:
        print("FizzBuzz")
    elif x % fizz == 0:
        print("Fizz")
    elif x % buzz == 0:
        print("Buzz")
    else:
        print(x)