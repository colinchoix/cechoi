import sys
import random
import re


def testSample(text, sampleLen):
    nText = text
    sample = random.randint(0,len(nText)-sampleLen)
    sampleText = nText[sample:sample+sampleLen]

    return len(set(sampleText))


text = open(sys.argv[1]).read()
nText = re.sub(r'[^A-Z]', '', text.upper())

avg = 0
large = 0 
for x in range(1000000):
    random.seed(x)
    result = testSample(nText, 87)
    #print(result)
    if result > 24:
        large += 1
    avg += result

print(avg/1000000)
print(large)
print(large/1000000)