import fileinput
import random

def genRandKey(text):
    '''
    Given a List of characters as input ["A", "AB", "B123", "AB"] return a random dorabella key 
    A key is a dictionary of the characters from the input list as the key and a mapping to a dorabella characters for the value
    '''

    letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X']
    characters = set(text)
    key = {}

    random.shuffle(letters)
    for i, c in enumerate(characters):
        key[c] = letters[i] 

    return key

if __name__ == "__main__":

    for line in fileinput.input():
        key = genRandKey(line)

        encodedText = "".join([key[i] for i in line])

        print(encodedText, end="")