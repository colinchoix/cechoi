#!/bin/bash


files=LetterSamples/*

for file in $files
do
    cat $file | python3 encodeMASC.py > encodedLetters/${file:14}.out
done