import sys
import numpy as np
import random
import kenlm

LETTERS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X']
ARPA = open(sys.argv[1]).read()
nGrams = {}
np.random.seed(42)

def arpaToDict(arpa):
    '''
    Take an Arpa file and return a dictionary representation
    '''
    
    arpa = arpa.split("\n\n")[1:-1]
    arpa = [x.split("\n")[1:] for x in arpa]

    nGrams = {}

    for n, grams in enumerate(arpa, 1):
        nGrams[n] = {}

        for gram in grams:
            values = gram.split("\t")

            if len(values) == 3:
                nGrams[n][values[1]] = [values[0],values[2]]
            else: 
                nGrams[n][values[1]] = [values[0]]

    return nGrams

def getNgramProb(ngram, backoff = False):
    n = len(ngram.replace(" ", ""))
    ngram[:n]
    ngram[-1]

    if ngram in nGrams[n]:
        if backoff:
            return nGrams[n][ngram][1]
        else:
            return nGrams[n][ngram][0]
    else:
        pass

def getCharProbVectors(sentence, n):
    pass

def nextRandomCharacter(sentence, n):
    

    characters = np.array(characters)
    probability = np.array(probability)
    probability /= probability.sum()

    nextChar = np.random.choice(characters, p=probability)

    return nextChar
    

if __name__ == "__main__":
    nGrams = arpaToDict(ARPA)

    #print(nGrams[1])
    print(getNgramProb("A B", True))