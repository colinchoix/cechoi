import sys
import kenlm
import random

engModel = kenlm.LanguageModel("frank.arpa")
doraModel = kenlm.LanguageModel("doraLines.arpa")

english = re.sub(r'[^A-Z ]', '', open("test/Frankenstein.txt").read().upper())
english2 = re.sub(r'[^A-Z ]', '', open("test/Dracula.txt").read().upper())
dorabella = re.sub(r'[^A-Z ]', '', open("test/dorabella.txt").read().upper())
randBella = re.sub(r'[^A-Z ]', '', open("test/randBella.txt").read().upper())
random = re.sub(r'[^A-Z ]', '', open("randomText.txt").read().upper())

sampleLen = 87

sample = random.randint(0,len(english)-sampleLen)
sampleText = english[sample:sample+sampleLen]
print(engModel.score(sampleText))
print(engModel.perplexity(sampleText))

'''
sentence = "A B C D E F G D H A I J K L J M J J F B B J N G O G N I P G J G F Q D H R S C J J C F N K G J I J F T P K L Q H H Q I P C P F U P C L U U N P C J F U K P N D B N P F D L E D"
print(model.score(sentence))
print(model.perplexity(sentence))
print()

sentence = "P E J V J P C P N J N J E A L P Q I P C J X O G N N J H T P K P N D B S E U E P F F H D P A P J P H I P P F T K P P D V P P L J M J J D P R C H Q P Q N A D S C J F T P C J G"
print(model.score(sentence))
print(model.perplexity(sentence))
print()

sentence = "S R K W L E X S U H B W W W N T A M Q S C R B U J K O A T V K W G I I T A O O A P I X L R C I L K Q H R C I U X D P K W S R Q D M U S V C F X M K I Q K U U Q W I N Q H E B E"
print(model.score(sentence))
print(model.perplexity(sentence))
print()

sentence = "G T H G F D B P H J C V Q H Q H R G O G G F N K D N G B F B F D B T V P N D K D E G D S C H R G C F V P T O I T K L E F Q I O B J N G M J W P F B O K K M J J F O J J C L T H"
print(model.score(sentence))
print(model.perplexity(sentence))
print()

sentence = "J N B I M C B M N H G O G N G P J F C N H P F T P T I P N S O U T P J I P I P P P P F C X D P P I P R P R G G O G C D E P D D P B B J N D G L Q T P H R S C J L Q H E F I P V"
print(model.score(sentence))
print(model.perplexity(sentence))
print()
'''