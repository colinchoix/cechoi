import sys
import numpy as np
import random

LETTERS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X']
ARPA = open(sys.argv[1]).read().split("\n")
np.random.seed(42)

def getNgram(N):
    '''Parse out ngrams from arpa file
    { ngram : [probability, backoff probability] }
    '''

    nGrams = {}

    numNgrams = ARPA[N].split('=')[1]

    for n, line in enumerate(ARPA):
        if(line[0:2] == '\\'+ str(N)):
            for ng in ARPA[n+1:n+int(numNgrams)+1]:
                ns = ng.split("\t")
                nGrams[ns[1]] = [ns[0],ns[2]]
    
    return nGrams

def getNgramProb(N):
    '''
    returns 3 vectors for characters, probability and back off probability
    '''

    ng = getNgram(N)
    
    characters = []
    probability = []
    BOprobability = []

    for x in ng:
        if x == '<s>':
            continue
        characters.append(x)
        probability.append(10**float(ng[x][0]))
        BOprobability.append(10**float(ng[x][1]))
    
    characters = np.array(characters)
    probability = np.array(probability)
    BOprobability = np.array(BOprobability)

    probability /= probability.sum()

    return [characters, probability, BOprobability]

def getProbability(ngram, BO = 0):
    '''
    return the probability or backoff probability of an ngram
    TODO: get it to work with higher N
    '''

    N = len(ngram.replace(" ", ""))
    ng = getNgram(N)
    
    if ngram in ng:
        return 10**float(ng[ngram][BO])
    else: 
        return 10**float(ng['<unk>'][BO])

    return -1

def getTProbability(ngram, BO = 0):
    ng = getNgram(2)
    
    if ngram in ng:
        return 10**float(ng[ngram][BO])
    else: 
        #TODO: CHECK THIS
        return getProbability(ngram[0],1) * getProbability(ngram[-1],1)

    return -1

def randomUnigram():
    characters, probability, _ = getNgramProb(1)

    nextChar = np.random.choice(characters, p=probability)

    if nextChar == '<unk>':
        return random.choice(["V", "W", "X"])
    else: 
        return nextChar

def randomBigram(sentence):
    # First letter is a random unigram
    if (sentence == ""):
        return randomUnigram()

    bigrams = getNgram(2)
    characters, probability  = [], []

    # Add charaters and probabilities for bigrams in the arpa to our vectors
    for x in bigrams:
        if x[0] == sentence[-1]:
            characters.append(x[-1])
            probability.append(10**float(bigrams[x][0]))

    # Calculate the probability for charaters not in our arpa using back off probabilities
    for l in LETTERS:
        if l not in characters:
            characters.append(l)
            probability.append(getProbability(l) * getProbability(sentence[-1], 1))

    characters = np.array(characters)
    probability = np.array(probability)
    probability /= probability.sum()

    nextChar = np.random.choice(characters, p=probability)

    return nextChar

def randomTrigram(sentence):
    # First letter is a random unigram
    if (sentence == ""):
        return randomUnigram()
    
    # Second letter is a random bigram
    if (len(sentence) == 1):
        return randomBigram(sentence)

    Trigrams = getNgram(3)
    characters, probability  = [], []

    # Add charaters and probabilities for Trigrams in the arpa to our vectors
    for x in Trigrams:
        if x[:3] == sentence[-3:]:
            characters.append(x[-1])
            probability.append(10**float(Trigrams[x][0]))

    # Calculate the probability for charaters not in our arpa using back off probabilities
    for l in LETTERS:
        if l not in characters:
            characters.append(l)
            probability.append(getProbability(l) * getTProbability(sentence[-3:], 1))

    characters = np.array(characters)
    probability = np.array(probability)
    #print(probability.sum())
    probability /= probability.sum()

    nextChar = np.random.choice(characters, p=probability)

    return nextChar

def generateUnigram(length):
    randomText = ""

    for _ in range(length):
        randomText += randomUnigram() + " "

    return randomText[:-1]

def generateBigram(length):
    randomText = ""

    for _ in range(length):
        randomText += randomBigram(randomText[:-1]) + " "

    return randomText[:-1]

def generateTrigram(length):
    randomText = ""

    for _ in range(length):
        randomText += randomTrigram(randomText[:-1]) + " "

    return randomText[:-1]

def generateRandom(length):
    randomText = ""

    for _ in range(length):
        randomText += random.choice(LETTERS) + " "

    return randomText[:-1]

def createTestFiles(files, length, textGenerator, name = "test", directory = ""):
    for x in range(files):
        np.random.seed(x)
        random.seed(x)
        filename = directory + name + str(x)
        f = open(filename, "w")
        content = textGenerator(length)
        f.write(content)
        f.close()


if __name__ == "__main__":

    '''
    test = open("unitest.txt").read()
    for x in LETTERS:
        print(x ,getProbability(x), test.count(x)/500000)

    '''
    print(generateBigram(87))
    #print(generateRandom(500000))
    '''
    createTestFiles(1000, 400000, generateUnigram, "randomDorabella", "randomDorabella/")
    createTestFiles(1000, 400000, generateRandom, "randomText", "randomText/")
    '''