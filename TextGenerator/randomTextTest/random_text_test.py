import sys
import kenlm
import random
import re

def test(Text, Model, sampleLen, runs, seed = 42):
    random.seed(seed)

    avgScore = 0
    avgPerplexity = 0

    for _ in range(runs):
        sample = random.randint(0,len(Text)-sampleLen)
        sampleText = Text[sample:sample+sampleLen]

        avgScore += Model.score(sampleText)
        avgPerplexity += Model.perplexity(sampleText)

    print(avgScore/runs, avgPerplexity/runs, sep = "\n", end="\n\n")


if __name__ == "__main__":
    # Models
    engModel = kenlm.LanguageModel("frank.arpa")
    doraModel = kenlm.LanguageModel("doraLines.arpa")
    doraModel2 = kenlm.LanguageModel("dorabellaTriLines.arpa")

    # Texts
    english = re.sub(r'[^A-Z ]', '', open("test/Frankenstein.txt").read().upper())
    english2 = re.sub(r'[^A-Z ]', '', open("test/Dracula.txt").read().upper())
    dorabella = re.sub(r'[^A-Z ]', '', open("test/dorabella.txt").read().upper())
    randBella = re.sub(r'[^A-Z ]', '', open("test/randomBella.txt").read().upper())
    randBella2 = re.sub(r'[^A-Z ]', '', open("test/randomBella2.txt").read().upper())
    randomText = re.sub(r'[^A-Z ]', '', open("test/randomText.txt").read().upper())

    # Parameters
    sampleLen = 87
    runs = 1000000

    # English English
    test(english, engModel, sampleLen, runs)

    # English2 English
    test(english2, engModel, sampleLen, runs)

    # dorabella English
    test(dorabella, engModel, sampleLen, runs)

    # randBella English
    test(randBella, engModel, sampleLen, runs)

    # randBella2 English
    test(randBella2, engModel, sampleLen, runs)

    # randomText English
    test(randomText, engModel, sampleLen, runs)

    print("-----------------------------------")

    # English Dorabella
    test(english, doraModel, sampleLen, runs)

    # english2 Dorabella
    test(english2, doraModel, sampleLen, runs)

    # dorabella Dorabella
    test(dorabella, doraModel, sampleLen, runs)

    # randBella Dorabella
    test(randBella, doraModel, sampleLen, runs)

    # randBella Dorabella
    test(randBella2, doraModel, sampleLen, runs)

    # randomText Dorabella
    test(randomText, doraModel, sampleLen, runs)

    print("-----------------------------------")

    # English Dorabella
    test(english, doraModel2, sampleLen, runs)

    # english2 Dorabella
    test(english2, doraModel2, sampleLen, runs)

    # dorabella Dorabella
    test(dorabella, doraModel2, sampleLen, runs)

    # randBella Dorabella
    test(randBella, doraModel2, sampleLen, runs)

    # randBella Dorabella
    test(randBella2, doraModel2, sampleLen, runs)

    # randomText Dorabella
    test(randomText, doraModel2, sampleLen, runs)