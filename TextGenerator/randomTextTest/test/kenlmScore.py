# created arpa model with bin/lmplz -o 5 --discount_fallback <dorabella_lines.txt >dorabellal.arpa
import kenlm
import sys

model = kenlm.LanguageModel(sys.argv[1])
text = open(sys.argv[2]).read()
print('{0}-gram model {1}\n'.format(model.order, sys.argv[1]))


if __name__ == "__main__":
    perplexity = model.perplexity(text)
    score = model.score(text)

    print(perplexity, score)