#!/bin/bash

files=LettersUnravel/decipheredLetters/*

for file in $files
do
    #cat $file | python3 encodeMASC.py > encodedSamples/${file:12}.out textSamples/
    #echo ${file:32:-8}
    python3 decAccuracy.py $file LettersUnravel/LetterSamples/${file:33:-4}.* v >> LettersUnravel/norvig_results_v.txt
    python3 decAccuracy.py $file LettersUnravel/LetterSamples/${file:33:-4}.* kv >> LettersUnravel/norvig_results_k.txt
done

    python3 aggResults.py LettersUnravel/norvig_results_v.txt
    python3 aggResults.py LettersUnravel/norvig_results_k.txt

