import matplotlib.pyplot as plt
import numpy as np

# Average: 3.63509
frequency = [12533, 16942, 18048, 15859, 12077, 8634, 5823, 3844, 2393, 1406, 913, 531, 369, 230, 153, 112, 48, 41, 18, 7, 6, 4, 4, 2, 1, 1, 1]
x = np.arange(len(frequency))



fig, ax = plt.subplots()

ax.set_title('Number of Reflected Symbols (Music)')
ax.set_xlabel('Count')
ax.set_ylabel('Frequency')

plt.bar(x, frequency)
plt.xticks(x, ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '26', '28'))
plt.show()