import matplotlib.pyplot as plt
import numpy as np
from tests import *

#testThree("elgar/Elgar_great_is_the_lord.txt", 100000, 87)
#testFour("englishText/The_Adventures_of_Sherlock_Holmes.txt", 1000, 87)
#testFour("frenchText/letters_french.txt", 1000, 87)
frequency, labels = formatResults(testFour("frenchText/letters_french.txt", 100000, 87))
    
x = np.arange(len(frequency))

fig, ax = plt.subplots()

ax.set_title('Number of Reflected Symbols (French)')
ax.set_xlabel('Count')
ax.set_ylabel('Frequency')

plt.bar(x, frequency)
plt.xticks(x, labels)
plt.show()