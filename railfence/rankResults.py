import os
import kenlm
import sys

if __name__ == "__main__":
    model = kenlm.LanguageModel(sys.argv[1])
    textScore = []

    for filename in os.listdir("railDecipherments/"):
        path = "railDecipherments/"+filename
        text = open(path).read()
        perplexity = model.perplexity(text)

        textScore.append([perplexity, text, filename])

    textScore = sorted(textScore, key=lambda x: x[0])

    #output = open("sortedResults.txt","w")
    for c,x in enumerate(textScore,1):
        print("Rank "+str(c)+":", x[2][5:],"Perplexity:",x[0],"\n"+x[1])